# AB++

Afterbirth+ API Extension Framework

# What?

AB++ is a Lua module for Binding of Isaac: Afterbirth+.
It makes it possible to very easily add your own methods and properties to various classes.

Here's a taste:
```lua
-- load abpp...
function Vector.inst:__tostring()
  return "(" .. tostring(self.X) .. "," .. tostring(self.Y) .. ")"
end

function Vector.inst:Doubled()
  return Vector(self.X * 2, self.Y * 2)
end

test = Vector(0, 15)
Isaac.DebugString(tostring(test)) -- prints: (0.0,15.0)
Isaac.DebugString(tostring(test:Doubled())) -- prints: (0.0, 30.0)
```

# Downloading

Right click on a link -> `Save link as...` (or equivalent)

* **[compiled Lua script](https://gitlab.com/Zatherz/abpp/raw/master/ab++.lua)** (you probably want this one)
* [original Moonscript source code](https://gitlab.com/Zatherz/abpp/raw/master/ab++.moon)

# Installing

If you're using my [LuaMerger](https://gitlab.com/zatherz) or [/u/Asterne](https://reddit.com/user/Asterne)'s [Modrequire](https://github.com/baskervald/modrequire),
just add this line at the top and put `ab++.lua` in the source code dir:  
```lua
abpp = require("ab++")
```

If you're not using any of those (you should try!), just copy the content of `ab++.lua` and paste them in between:  
```lua
abpp = (function()
```
and  
```lua
end)()
```

in the beginning of your `main.lua`.

# How do you use it?

By default, when you load the module (or if you just paste it at the top of your `main.lua`),
it's going to find every single class in any global and unrestrict it, making it possible to add fields and properties.

If you want to change this behavior, just set the `abpp_no_unrestrict` **global** variable to `true`.

An unrestricted class, apart from the low level write support, also has a nice API.

Note: the examples below use a function called `p`:  
```lua
function p(x)
  Isaac.DebugString(tostring(x))
end
```

### Static Field

To add a static field, just add a field to the class of your choice:  
```lua
Vector.TEST = 42

p(Vector.TEST) -- prints: 42
```

Since functions are values in Lua, you can also make it a function:  
```lua
function Vector.Test(num)
  return 42 * num
end

p(Vector.Test(2)) -- prints: 84
```

To make a method, just use the `:` syntax for both defining and calling it:  
```lua
function Vector.Test(num)
  return 2 * num
end

function Vector:Test2(num)
  return self.Test(num) * 42
end

p(Vector:Test2(2)) -- prints: 168
```
Note that this is considered bad practice.

### Instance Fields

To add an instance field, just put it in `Class.inst` instead of `Class`:  
```lua
Vector.inst.Test = 5

p(Vector(0, 0).Test) -- prints: 5
```

Of course, you can add functions and methods here. However, in this case,
using *functions* is considered bad practice and methods good practice.

**NOTE!** These fields are *not* saved in the C++ side, which means that
if you pass the object to a function from C++ that creates a new object based on
the values of the first object, the field is going to be reset to default.

Functions:  
```lua
function Vector.inst.TestFunction(num)
  return num * 2
end

p(Vector(0, 0).TestFunction(3)) -- prints: 6
```

Methods:  
```lua
function Vector.inst:TestMethod()
  return self.X * self.Y
end

p(Vector(3, 6):TestMethod()) -- prints: 18
```

### Static Properties

Static properties let you control when the user tries to set and/or get a property.  
To add a static property, just add a table to `Class.prop` that contains either `get`, `set` or both keys pointing to functions.  
```lua
Vector.prop.Test = {
  get = function(self)
    return 42
  end
}

p(Vector.Test) -- prints: 42
Vector.Test = 15 -- ERROR! no writable variable 'Test'
p(Vector.Test) -- doesn't get here
```

```lua
local x = 0
Vector.prop.Test = {
  get = function(self)
    return x
  end,
  set = function(self, val)
    x = val * 2
  end
}

p(Vector.Test) -- prints: 0
Vector.Test = 15
p(Vector.Test) -- prints: 30
```

### Instance Properties

Identical to static properties with the exception that you use `inst_prop`, not `prop`.

```lua
Vector.inst_prop.Test = {
  get = function(self)
    return 42
  end
}

v = Vector(0, 0)
p(v.Test) -- prints: 42
v.Test = 15 -- ERROR! no writable variable 'Test'
p(v.Test) -- doesn't get here
```

```lua
Vector.inst.__test = 0
-- using instance fields to store changeable data - not the best idea!
-- read the note in the Instance Fields section to learn more

Vector.inst_prop.Test = {
  get = function(self)
    return self.__test
  end,
  set = function(self, val)
    self.__test = val * 2
  end
}

v = Vector(0, 0)
p(v.Test) -- prints: 0
v.Test = 15
p(v.Test) -- prints: 30
```
