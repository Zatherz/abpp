module = {}

UNRESTRICTED_NEW_INDEX = (key, val) =>
  self_mt = getmetatable(@)
  propset = rawget(self_mt, "__propset")
  parent = rawget(self_mt, "__parent")
  parent_propset = rawget(parent, "__propset") if parent

  if parent_propset and parent_propset[key]
    return parent_propset[key] @, val
  if propset and propset[key]
    return propset[key] @, val

  -- __class is the instances' metatable
  -- but at the same time we want to allow __class modifications
  -- so we use this as a hack instead - just detect if self is userdata
  if type(@) == "userdata"
    error "no writable variable '" .. tostring(key) .. "'"

  rawset @, key, val

UNRESTRICTED_INDEX = (key) =>
  self_mt = getmetatable(@)
  propget = rawget(self_mt, "__propget")
  const = rawget(self_mt, "__const")
  parent = rawget(self_mt, "__parent")
  parent_propget = rawget(parent, "__propget") if parent

  if propget and propget[key]
    return propget[key] @
  if parent_propget and parent_propget[key]
    return parent_propget[key] @
  if parent and rawget(parent, key)
    return rawget parent, key
  if rawget(self_mt, key)
    return rawget self_mt, key
  if const and rawget(const, key)
    return const[key]

module.unrestrict = (klass) -> -- NOBODY TELL NICALIS! ;)
  class_mt = getmetatable klass
  inst_mt = getmetatable class_mt.__class

  if class_mt
    class_mt.__newindex = UNRESTRICTED_NEW_INDEX
    class_mt.__index = UNRESTRICTED_INDEX
    klass.prop = setmetatable {}, {
      __index: (key) =>
        {
          get: class_mt.__propget[key]
          set: class_mt.__propset[key]
        }

      __newindex: (key, val) =>
        if not (type(val) == "table" and (type(val.get) == "function" or type(val.set) == "function"))
          error "Must be a table with at least one function field out of `get` or `set`"

        class_mt.__propget[key] = val.get
        class_mt.__propset[key] = val.set
    }

  
  if inst_mt
    inst_mt.__newindex = UNRESTRICTED_NEW_INDEX
    inst_mt.__index = UNRESTRICTED_INDEX
    klass.inst = setmetatable {}, {
      __index: (key) =>
        class_mt.__class[key]

      __newindex: (key, val) =>
        class_mt.__class[key] = val
    }

    klass.inst_prop = setmetatable {}, {
      __index: (key) =>
        {
          get: class_mt.__class.__propget[key]
          set: class_mt.__class.__propset[key]
        }

      __newindex: (key, val) =>
        if not (type(val) == "table" and (type(val.get) == "function" or type(val.set) == "function"))
          error "Must be a table with at least one function field out of `get` or `set`"

        class_mt.__class.__propget[key] = val.get
        class_mt.__class.__propset[key] = val.set
    }
    

  klass.__unrestricted = true

module.unrestrict_all = (obj, name, cache) ->
  obj or= _G
  cache or= {}

  return obj if cache[obj]

  if type(obj) == "table"
    cache[obj] = true

    for k, v in pairs obj
      module.unrestrict_all v, k, cache

    mt = getmetatable(obj)
    if mt and mt.__class and mt.__class.__type
      -- it's a class!
      module.unrestrict obj

  obj

module.unrestrict_all! if not _G.abpp_no_unrestrict

module
